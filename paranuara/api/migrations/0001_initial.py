# Generated by Django 3.2.5 on 2021-07-25 17:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Company",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("company_name", models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name="Employee",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=50)),
                ("age", models.CharField(max_length=10)),
                ("address", models.CharField(max_length=500)),
                ("phone", models.CharField(max_length=30)),
                ("has_died", models.BooleanField()),
                ("eye_color", models.CharField(max_length=20)),
                ("fruits", models.TextField(null=True)),
                ("vegetable", models.TextField(null=True)),
                (
                    "company",
                    models.ForeignKey(
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="api.company",
                    ),
                ),
                (
                    "friends",
                    models.ManyToManyField(
                        related_name="_api_employee_friends_+",
                        to="api.Employee",
                    ),
                ),
            ],
        ),
    ]
