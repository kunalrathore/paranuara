# Generated by Django 3.2.5 on 2021-07-25 17:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("api", "0001_initial")]

    operations = [
        migrations.AlterField(
            model_name="employee",
            name="friends",
            field=models.ManyToManyField(to="api.Employee"),
        )
    ]
