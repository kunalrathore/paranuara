import json

from rest_framework import status
from rest_framework.test import APITestCase

from api.management.commands import loaddata
from .models import Company, Employee
from .serializers import EmployeeSerializer, PeopleSerializer


class APITest(APITestCase):
    """
        APITest : for testing API endpoints.
    """
    def setUp(self):
        cmd = loaddata.Command()
        opts = {"size" : 10}
        cmd.handle(**opts)

    def test_company_employee(self):
        """
        test_company_employee : test /company/<name>/ endpoint
        """
        respone = self.client.get("/company/NETBOOK")
        company_object = Company.objects.get(company_name="NETBOOK")
        employees = company_object.employee_set.all()
        serialized_employee = EmployeeSerializer(employees, many=True)
        self.assertEqual(respone.data, serialized_employee.data)
        self.assertEqual(respone.status_code, status.HTTP_200_OK)

    def test_employee_detail(self):
        """
        test_company_detail : test /people/<pk>/ endpoint
        """
        respone = self.client.get("/people/1")
        people_object = Employee.objects.get(id=1)
        serialized_people = PeopleSerializer(people_object)
        self.assertEqual(
            respone.content.decode("UTF-8"), json.dumps(serialized_people.data)
        )
        self.assertEqual(respone.status_code, status.HTTP_200_OK)

    def test_employee_detail_common_friends(self):
        """
        test_employee_detail_common_friends : test /people/<pk>/<pk>/ endpoint
        """
        respone = self.client.get("/people/1/2")
        obj1 = Employee.objects.get(id=1)
        obj2 = Employee.objects.get(id=2)
        obj1_friends = obj1.friends.values_list("pk", flat=True)
        mutual_friends = obj2.friends.filter(
            pk__in=obj1_friends, has_died=False, eye_color="brown"
        )
        serialised = PeopleSerializer(mutual_friends, many=True)
        person1 = PeopleSerializer(obj1)
        person2 = PeopleSerializer(obj2)
        data = {
            "Person 1": person1.data,
            "Person 2": person2.data,
            "Alive Common Friends with Brown Eyes": serialised.data,
        }
        self.assertEqual(eval(respone.content.decode("UTF-8")), data)
        self.assertEqual(respone.status_code, status.HTTP_200_OK)
