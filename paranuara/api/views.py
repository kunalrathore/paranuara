import json

from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework import generics
from rest_framework.views import APIView

from .models import Company, Employee
from .serializers import EmployeeSerializer, PeopleSerializer


class CompanyViewSet(generics.ListAPIView):
    """
        Return details of employees working in a company.
    """
    model = Company
    serializer_class = EmployeeSerializer

    def get_queryset(self, *args, **kwargs):
        """
        get_queryset : customise queryset
        """
        company = self.kwargs["name"]
        company_object = get_object_or_404(Company, company_name=company)
        employees = company_object.employee_set.all()
        return employees


class EmployeeViewSet(APIView):
    """
        Return details of Employee like name, age, fruits,
        vegetables.
    """
    def get(self, request, *args, **kwargs):
        """
        get : Person Details
        """
        employee_id = self.kwargs["pk"]
        obj = get_object_or_404(Employee,id=employee_id)
        serialised = PeopleSerializer(obj)
        return HttpResponse(json.dumps(serialised.data))


class ListUsers(APIView):
    """
        Return detail of both users and list of their common
        friends with brown eyes and they are still alive.
    """
    def get(self, request, *args, **kwargs):
        """
        get : Details of both users along with their comman friends with brown eyes and still alive.
        """
        employee_id1 = self.kwargs["pk1"]
        employee_id2 = self.kwargs["pk2"]
        obj1 = get_object_or_404(Employee,id=employee_id1)
        obj2 = get_object_or_404(Employee,id=employee_id2)
        obj1_friends = obj1.friends.values_list("pk", flat=True)
        mutual_friends = obj2.friends.filter(
            pk__in=obj1_friends, has_died=False, eye_color="brown"
        )
        serialised = PeopleSerializer(mutual_friends, many=True)
        person1 = PeopleSerializer(obj1)
        person2 = PeopleSerializer(obj2)
        data = {
            "Person 1": person1.data,
            "Person 2": person2.data,
            "Alive Common Friends with Brown Eyes": serialised.data,
        }
        return HttpResponse(json.dumps(data))
