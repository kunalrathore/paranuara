from rest_framework import serializers

from .models import Company, Employee


class EmployeeSerializer(serializers.ModelSerializer):
    """
    EmployeeSerializer : fields all
    """
    class Meta:
        model = Employee
        fields = "__all__"


class PeopleSerializer(serializers.ModelSerializer):
    """
    PeopleSerializer : fields id, name, age, address, phone, fruits, vegetable
    """
    class Meta:
        model = Employee
        fields = ("id", "name", "age", "address", "phone", "fruits", "vegetable")


class CompanySerializer(serializers.ModelSerializer):
    """
    CompanySerializer : fields all
    """
    class Meta:
        model = Company
        fields = "__all__"
