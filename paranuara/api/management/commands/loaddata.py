import json

from django.conf import settings
from django.core.management.base import BaseCommand

from api.constants import STORE
from api.models import Company, Employee


class Command(BaseCommand):
    def handle(self, size = 0, *args, **kwargs):
        with open(settings.BASE_DIR / "resources/companies.json") as company_file:
            companies = json.load(company_file)
            for company in companies:
                Company.objects.create(
                    **{
                        "id": company["index"] + 1,
                        "company_name": company["company"],
                    }
                )
        with open(settings.BASE_DIR / "resources/people.json") as employee_file:
            employees = json.load(employee_file)
            if size != 0: employees = employees[:size]
            for employee in employees:
                # STORE contains harcoded names of fruits and vegetables
                # Need to update STORE if getting new data
                items = {"fruit": [], "vegetable": []}
                for item in employee["favouriteFood"]:
                    items[STORE[item]].append(item)
                Employee.objects.create(
                    **{
                        "id": employee["index"] + 1,
                        "name": employee["name"],
                        "age": employee["age"],
                        "address": employee["address"],
                        "phone": employee["phone"],
                        "has_died": employee["has_died"],
                        "eye_color": employee["eyeColor"],
                        "fruits": items["fruit"],
                        "vegetable": items["vegetable"],
                        "company_id": employee["company_id"],
                    }
                )
            if size != 0: employees = employees[:size//2]
            for employee in employees:
                index = employee["index"] + 1
                emp = Employee.objects.get(id=index)
                for frnd in employee["friends"]:
                    index = frnd["index"] + 1
                    temp_emp = Employee.objects.get(id=index)
                    emp.friends.add(temp_emp)
