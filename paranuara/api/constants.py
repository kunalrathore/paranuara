STORE = {
    "cucumber": "vegetable",
    "beetroot": "vegetable",
    "carrot": "vegetable",
    "celery": "vegetable",
    "orange": "fruit",
    "apple": "fruit",
    "banana": "fruit",
    "strawberry": "fruit",
}
