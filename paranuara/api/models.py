from django.db import models


class Company(models.Model):
    """
    Company: properties- company_name
    """
    company_name = models.CharField(max_length=30)


class Employee(models.Model):
    """
    Employee: properties- name, age, address, phone, has_died, eye_color, fruits, vegetable, friends, company
    """
    name = models.CharField(max_length=50)
    age = models.CharField(max_length=10)
    address = models.CharField(max_length=500)
    phone = models.CharField(max_length=30)
    has_died = models.BooleanField()
    eye_color = models.CharField(max_length=20)
    fruits = models.TextField(null=True)
    vegetable = models.TextField(null=True)
    friends = models.ManyToManyField("self", symmetrical=False)
    company = models.ForeignKey(Company, on_delete=models.SET_NULL, null=True)
